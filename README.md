# Braam.io

This is a rewrite of the existing squarespace site.

<!-- TODO Add more details and the instruction as per upwork tasks in D -->

For now this contains a running stream of thoughts related to the
implementation.
This will be formalized later.

After experimenting with a bunch of integrations, I have settled on a modified
`pointbubbl` theme. Several pull requests are pending, barring which I will have
to fork or work locally.



**To make posts** check the `docs` folder, and the [PDF therein](docs/blogDoc.pdf).

## Installation Instructions

### On Mac

```
# brew install hugo
# brew install yarn
# git clone git@gitlab.com:braamioweb/braamioweb.gitlab.io.git
# git branch -r  (see available branches)
# git checkout dev (continue work with the dev branch)
# cd braamioweb.gitlab.io
# yarn
# hugo server -D
```

### Version names and branching

* We organize work towards releases on branches `v0` (our starting point), `v1` (first version to go live), `v2` etc.
* When a particular version of a branch can be released, we **tag** it with `p3.0.7` (`7`-th attempt for `0`-th fix of version `v3`).  
* `px.y.z` is the tag for the `y`-th fix on version `x`, and the `z`-th attempt to get it right.  
* The git tag `production` will point to a `px.y.z` at any time


### CI


It will do the following
* build the `production` tag and place it on [https://braamioweb.gitlab.io/]

In the future we additionally want:
* build development branches at [https://braamioweb.gitlab.io/v2]
* this is close to being available from gitlab, see https://gitlab.com/gitlab-org/gitlab-ce/issues/35141



-------

### Depreciated


### for CI

Presently, since the modifications require `sass`, it is best to simply use
`snap` to install `hugo` as mentioned [here](https://gohugo.io/getting-started/installing/#snap-package).

``` bash
snap install hugo --channel=extended
```

Luckily CI systems also allow for `snap` installations so this will not be a
problem during production.




## Banner Modifications


The banner content is in `partials/hero/home` and accepts valid HTML.
Do note that size and formatting of the same is specified by a `div` in
`partials/hero/banner`.

Typically the only changes envisioned here is a set of `<h1></h1>` content. 

This was found to be more complex than required.

### Present implementation

The `banner.md` file under `content/home_pages/` is a markdown formatted file.

## Content Blocks

Each block is hosted under `partials/home/` with the relevant data taken from `data/home/`

## Projects

These blocks are auto-generated from the `data/home/projects/` `.yml` files. In
essence, the number of responsive column cards changes as more are added. The
content in the `yml` file may include markdown (see `ska.yml`).

``` yaml
title: Necessary
subtitle: Optional
img: Necessary
alt: Optional
desc: Necessary
```

The images for the same are expected to reside in `static/img` and are to be
referenced as `img/name.ext` where `ext` may be `png` or `jpg` or any standard format.

## About Section

This block takes content from `data/home/aboutPeter.yml`. The most important
thing to remember about this section is that the data in the `yml` file will
render each sub-item in `desc` as a paragraph.

``` yaml
img: Necessary
desc:
  - "Para 1"
  - "Para 2"
```

## Speaking Section

This portion is almost identical to the previous in terms of implementation.

## Contact Section

This section is rather strongly typed in HTML. Mostly the only configuration
exposed is in `config.toml`.

### Staticman Gotchas
This section briefly describes several issues with staticman and their fixes.

#### Yaml sections

No duplicated entries are allowed (naturally) and moreover, the `form` `POST
url` must be written to include the relevant section in the `yml** file.

**MOST IMPORTANTLY.**
Staticman *DOES NOT* read local changes to the `staticman.yml` file. This means
that all changes will only be picked up by the bot when the changes are pushed
to github.

This actually makes a lot of sense because it's a github bot. Nevertheless it
seems important to note.

## The Menu
The menu is configured with `config.toml` and the `content` directories.
