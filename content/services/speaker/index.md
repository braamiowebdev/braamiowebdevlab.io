---
title: "Speaking"
date: Sun Jan 14 03:39:34 2018 
draft: false
authors: 
  - Peter Braam
categories: 
  - services
tags: 
show_gallery: false
resources:
  - src: "images/speaker.png"
    title:
    params:
---

I give several invited lectures each year.  I focus on science and technology, with a dose of reality when it is called for.  I try to be objective, tune the content
carefully to the audience and usually present both surprising content and
statements that make people laugh. Please get in touch if you'd like me to
speak. 

### Speaker Bio

Peter Braam is a scientist and technologist.  He received a DPhil in mathematics at Oxford where he subsequently held postdocs and faculty positions, focussing on geometry, topology and theoretical physics.   In 1997 he joined CMU as a computer scientist.  Peter created the Lustre file system, which has become a key product for large scale HPC.  Before returning to academic work in 2013 he founded 6 startups, 4 of which exited successfully, and he held senior executive positions in major computing companies.  From 2013, Peter has been a consultant and visiting academic to Cambridge University to support the architecture of the SKA telescope. He researches parallel computing and theoretical aspects of machine learning.  He is a Visiting Professor of Physics at Oxford University and a Visiting Scholar at the Flatiron Institute in New York.

### Recent Lectures

Several Recent Lecture are on my [Blog](https://www.braam.io/blog)

Lectures given during the last few years include:

 - 2019
   - Keynote at Conference for Next Generation Arithmetic.  The SKA Compute Challenge and Number Formats.
 - 2018
   - CHPC 2018 TensorFlow as an HPC InfraStructure
   - ICPE 2018 Berlin. Performance Engineering for the SKA Telescope
 - 2017 
   - Storage for High Performance Computing 2000 - 2025 