---
title: "Work Experience"
date: 2018-06-07T20:25:37+10:00
draft: false
authors: 
  - Peter Braam
show_gallery: false
---

## Consultanties as Thought Leader and Thinking Partner

 - **2018 European Processor Initiative (EPI):**  I assisted one of the partners in the EPI consortium with review of proposals and evaluation of critical technology.
 - **2013- Square Kilometer Array (SKA) radio telescope:** As a consultant to Cambridge University, I contributed to technical, vendor related and process oriented aspects of engineering for the SKA.
 - **2014 Department of Science and Technology (DST) South Africa:**  I evaluated proposals for the DST and assisted them brainstorming about critical initiatives.  I brought new areas of focus to the annual CHPC conference.
 - **2011 - 2013 Exascale I/O Workgroup:**  I led a discussion group which developed influential ideas.
 - **2012-2014, EC Brussels:**  I served on the committee to create initial blueprints for the Horizon 2020 program's HPC research initiatives.

## Executive

####  Xyratex 2010-2013.  

As Senior Vice President I was a member of the CEO's executive board and directed software engineering, in a team of some 300 engineers, across many products, including:

 - ClusterStor's product line
 - Grid RAID for Lustre
 - Secure Lustre
 - MERO/Colibri Object Storage System
 - Enclosure Firmware

I introduced architecture and software development processes.  I managed outsourcing relationships 
for software and was involved in the selection of oversearch design and manufacturing of hardware.

When it became apparent Xyratex was going to be acquired (by Seagate) I left the company.


#### SUN Microsysmstem, Vice President 2007-2008

While reporting to the Executive VP of Engineering (John Fowler) and working with the CEO (Jonathan Schwarz) I worked on strategic projects such as:

 - the integration of Cluster File Systems into SUN
 - architecture of a unified approach to SUN storage
 - ports of products from Linux to Solaris and vice versa

In many of these projects I worked with Jeff Bonwick and Bill Moore.  I visited many customers around the world and worked on proposals for HPC installations with Andreas Bechtolsheim. When it became apparent SUN was going to be acquired (by Oracle) I left the company.

#### CEO for 5 of my startups, (and CTO for one).

I led the architecture of products, managed early customer relations and technical marketing.  I leveraged processes from Carnegie Mellon's Software Engineering Institute.  I was coached to gain executive skills, which became valuable as the company grew to around 130 people and 100 customers. I generally wrote white papers and course materials and delivered many technical lectures.

## Academic

 - **Academic 2015-:** I interact with a few research groups on issues described on the project page.  I give some 5 invited lectures annually, and frequently initiate discussions about open issues that have not received enough attention.  I organize brainstorming meetings or small workshops about it.  Sometimes I publish a refereed paper, and guide some graduate students. I work both with academic and industry researchers.
 - **Senior Systems Scientist 1996-2000:** I took over leadership of the Coda project in the School of Computer Science at Carnegie Mellon University as a Senior Systems Faculty member.  The project and one of my papers won an award, and the origins of two successful commercial products (InterMezzo and Lustre) were established.
 - **Mathematician 1980-1996:** I pursued an academic career until 1995.  I published papers, had eight PhD students complete their thesis under me.
