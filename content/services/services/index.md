---
title: "Services - Intellectual Resonance"
draft: false
authors: 
  - Peter Braam
show_gallery: false
resources:
 - img: "img/resonance.jpg"
---

### Working with me

I offer consulting services to both industry and academia.  I will gladly consider both smaller and larger opportunities, and I can assemble a small team of collaborators rapidly.  Contact me if you are interested.
