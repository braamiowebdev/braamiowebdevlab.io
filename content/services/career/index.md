---
title: "Career Overview"
date: 2018-06-07T20:25:37+10:00
draft: false
authors: 
  - Peter Braam
show_gallery: false
resources:
 - img: "img/work-leader.jpg"
---

### 1996 - Entrepreneur

 - 2016 - 2017 Co-Founder, CEO. Campaign Storage, LLC.
 - 2011 - 2015 Founder, CEO, Parallel Scientific LLC & DocsForce
 - 2010 - 2013 Senior Vice President, Chief Software Architect, Fellow Xyratex Inc.
 - 2009 - 2010 Founder, CEO, Chairman ClusterStor LLC (acquired by Xyratex)
 - 2007 - 2008 Vice President of Lustre, SUN Microsystems
 - 2001 - 2007 Founder, CEO, Chairman, Cluster File Systems (acquired by SUN Microsystems)
 - 2000 - 2001 Co-Founder, VP Engineering, Mountain View Data, Inc. (assets acquired by NEC and others).
 - 2000 - 2001 Storage Architect, Red Hat Inc. 
 - 2000 - 2000 Chief Architect, TurboLinux
 - 1995 - 2000 Co-Founder, CEO. Stelias Computing, Inc. (acquired by TurboLinux).

### 1996 - Scientist

 - 2019 - Visiting Professor of Physics, Oxford University
 - 2018 - Visiting Scholar, Center for Computational Astrophysics, Flatiron Institute, NYC
 - 2015 - 2017 Visiting Scholar, Pembroke College, Cambridge University
 - 2013 - Principal, Braam Research, LLC
 - 2007 - 2010 Adjunct Professor Chinese Academy of Sciences
 - 1996 - 2005 Senior Systems Scientist (faculty position) School of Computer Science, Carnegie Mellon University

### 1980 - 1997 Mathematician

 - 1992 - 1993 Special Fellowship, Oxford University
 - 1992 Visiting Professor, University of British Columbia
 - 1991-1997 University Lecturer and Tutorial Fellow (tenured), St. Catherine's College, Oxford
 - 1988-1991 Assistant and (tenured) Associate Professor of Mathematics, University of Utah 
 - 1987-1990 Junior Research Fellow, Merton College Oxford
 - 1987-1990 Christian and Constantijn Huygens Fellow, National Dutch Science Foundation
 - 1985 University of Utrecht, Research Assistant
 - 1981-1984 University of Utrecht, Teaching Assistant

### Education
 
 - 1987 Oxford University DPhil in Mathematics, supervisor Sir Michael Atiyah
 - 1984 University of Utrecht MSc in Mathematics, supervisor Prof. J.J. Duistermaat
 - 2007 Software Engineering Institute, Carnegie Mellon University - Certified Software Architecture Professional
 - 2004 Software Engineering Institute, Carnegie Mellon University - Certified Team Software Process instructor and coach


