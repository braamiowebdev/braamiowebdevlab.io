---
title: "Dramatic Changes in HPC Storage"
date: 2017-12-26T06:25:37+10:00
draft: false 
authors:
  - Peter Braam
categories:
  - HPC
  - storage
show_gallery: false 
image: "images/intel-io-today-future.jpg"
resources:
  - src: "images/intel-io-today-future.jpg"
    title: 
    params:
---

# Dramatic Changes in HPC Storage

At the [2017 CHPC conference](https://events.chpc.ac.za/event/6/) I gave a plenary lecture looking at developments in HPC storage past and future.

Dramatic changes for Storage and IO in HPC are upon us. Bridging several tiers of storage is becoming a necessity, software will need to improve dramatically to keep up with faster memory and storage devices, and far more nodes will be supplying storage resources. We will talk about roles for technologies such as containers, file systems, object storage and give an overview of a broad class of new data movement software and analogies with the cloud. This is an area with many exciting opportunities and presently only a handful of solutions are available or under development.

Intel has published a beautiful image outlining some of the developments

{{< figure src="images/intel-io-today-future.jpg" width="600px" >}}

The numbers required by some of the future deployments, in particular the [SKA Science Data Processor](http://ska-sdp.org) always draw attention.

The slides for [CHPC IO](https://www.dropbox.com/s/gthbo4ha0scwng0/2017-12%20CHPC%20IO%20v2.pdf?dl=0). 

{{< figure src="images/chpc-2017-audience.jpg" width="600px" >}}
