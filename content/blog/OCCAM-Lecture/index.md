---
title: "OCCAM Lecture: Extreme Computing for SKA"
date: 2017-10-26T06:25:37+10:00
draft: false 
authors:
  - Peter Braam
categories:
show_gallery: false 
resources:
  - src: "images/GW-sky.jpg"
    title: 
    params:
---
The OCCAM lectures at [Merton College in Oxford](https://www.merton.ox.ac.uk) are given each Term.  In October 2017 I gave a talk about SKA, which led me to build relationships with the [Physics Department](https://www.physics.ox.ac.uk), and they helped me significantly in the organization of the workshop [AI at CERN and SKA](https://indico.cern.ch/event/745580/).  They also rewarded me with a wonderful tour of [CERN](https://home.cern/)

We have [slides here](XXX) and a movie was created, available from [Merton College's Occam Lecture page](http://merton.physics.ox.ac.uk/occam_lecture.html), and on [Youtube](https://www.youtube.com/watch?v=Cr4j9cPEFOw&feature=youtu.be).

