---
title: "Performance Engineering for the SKA Telescope"
date: 2018-04-10
draft: false 
authors:
  - Peter Braam
show_gallery: false 
image: "images/"
categories:
  - HPC
  - SKA
resources:
  - src: "images/SKA-3-aspects-Infographic_OP-rgb.screen.jpg"
    title: 
    params:
---

# Performance Engineering for the SKA telescope

I gave a keynote at [ICPE 2018 in Berlin](https://icpe2018.spec.org/)
about performance aspects in computing for the SKA telescope. The
slides were posted
[here](https://research.spec.org/fileadmin/user_upload/documents/icpe_2018/ICPE2018_PE_SKA_Telescope_Keynote_Peter_Braam.pdf) and the local copy is [here](https://www.dropbox.com/s/na7hfzfdr2blv3j/2018-04-12%20ICPE%20talk.pdf?dl=0).

## Abstract

The SKA radio telescope will be a massive world class scientific
instrument, currently under design by a world wide consortium, to
progress to full operation in South Africa and Australia in the mid
2020's.  The capabilities of the telescope are expected to enable
major scientific breakthroughs. At the center of its data processing
sits the Science Data Processor, a large HPC system with specialized
software.  In this lecture we will give a high level overview of the
project and progress to the computing and data related architecture.
Then we will discuss the work of the SDP design consortium to
understand and achieve the many performance requirements leveraging
hardware and algorithms.  Among these is a requirement for memory
bandwidth exceeding 100 PB/sec.

{{< figure src="images/SKA-3-aspects-Infographic_OP-rgb.screen.jpg" width="500px" >}}
