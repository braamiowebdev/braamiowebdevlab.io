---
title: "AI CERN SKA Workshop"
date: 2018-09-13T11:45:28+07:00
draft: false
images: "images/atlas-hl-lhc.png"

---
In September 2018 we had a wonderful workshop at the [Alan Turing Institute in London](https://www.turing.ac.uk/) concerning AI at CERN and SKA.

There were many wonderful presentations ranging from applications of AI to particle discovery at CERN, to deep mathematics concerning information extraction and very novel applications of ML to cast a dramatically new light on numerical solutions to physics PDE's - in this case for [structure formation in the early Universe](https://arxiv.org/abs/1811.06533).

The presentations can be found on the [Indico site](https://indico.cern.ch/event/745580/), no video recordings were made.
