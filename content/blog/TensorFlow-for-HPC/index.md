---
title: "TensorFlow for HPC?"
date: 2018-12-03
draft: false 
authors:
  - Peter Braam
show_gallery: false 
image: "images/"
categories:
  - HPC
  - SKA
resources:
  - src: "images/tpuv3.png"
    title: 
    params:
---

# TensorFlow for HPC

Google has developed [TensorFlow](https://tensorflow.org), a truly complete platform for ML.  TensorFlow has many ingredients, for example:

 - many domain specific libraries for machine learning
 - the TensorFlow domain specific data-flow language
 - carefully organized input and output for data flow
 - an optimizing runtime and compiler
 - hardware implementations of TensorFlow operations in TensorFlow processing unit (TPU) chips

The performance of the platform is amazing, and it begs the question if it will be useful for HPC in a similar manner that GPU's heralded a revolution.

[InsideHPC](https://insidehpc.com) created a [podcast](https://insidehpc.com/2019/02/video-tensor-flow-for-hpc/) based on my lecture about this at the South African National HPC Conference [CHPC 2018 Conference in Cape Town](https://chpcconf.co.za/). 

The slides are [here](https://chpcconf.co.za/files/2018/Peter_Braam.pptx) and [here](https://www.dropbox.com/s/p1u52kg4vx3egqu/2018-12%20TF%20system%20infrastructure.pdf?dl=0)

{{< figure src="images/tpuv3.png" width="500px" >}}
