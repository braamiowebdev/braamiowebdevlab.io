---
title: "Current Projects"
date: Sun Sep 16 03:39:34 2018
draft: false
authors: 
  - Peter Braam
---
{{% cols %}}
{{% col %}}
{{< figure src="img/aristotle.jpg" width="500px" >}}
{{% /col %}}

{{% col %}}

### 2019 Novel approaches to large scale scientific computing 
 - Between TensorFlow, particularly the TPU chips, limited precision arithmetic, more mathematics for program construction and data modeling, I believe there are opportunities for dramatic improvements for some grand challenge compute problems.

### 2018- Machine Learning
 - Applications of Machine Learning to simulation Physics Equations
 - Use of TensorFlow for High Performance Computing
 - Interpretability of ML models

### 2018- Limited Precision and Number Formats for SKA
 - The Role of Number formats and Precision for SKA compute pipelines

### 2017- Software for new Memory Technologies 
 - Concepts in programming languages and runtimes related to persistent and hybrid memory.

## 2014- Hardware and Software for Parallel Programming
 - Geometric underpinnings of efficient parallel algorithms, such as network
  topologies and data organization
{{% /col %}}

{{% /cols %}}
