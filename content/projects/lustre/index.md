---
title: "Storage - Lustre"
draft: false
authors: 
  - Peter Braam
resources:
---
{{% cols %}}
{{% col %}}
{{< figure src="img/storage.png" width="500px" >}}
{{% /col %}}

{{% col %}}

## Storage

I've worked on several storage systems during my life.  Here is a summary, and a collection of a few reference documents. 


### 1995-2000 Coda 

Coda developed at Carnegie Mellon by the group of Mahadev Satyanarayanan (Satya) was the first file system to deeply explore the concept of disconnected operation.   The [official Coda web page](http://coda.cs.cmu.edu) is here.

I led the effort from 1996 to 2001 and ported Coda to Linux.  I wrote an [overview article in the Linux Journal in 1998](https://www.dropbox.com/s/fzuzydvmui7e6ba/1998-coda-linux-journal.pdf?dl=0), with beautiful illustrations created by Gaich Muramatsu.

Michael Callahan initiated a near Herculean effort to port Coda to DOS (remember that?), we wrote [a paper about Coda on Windows](https://www.dropbox.com/s/vpo3s6mxn1zx1px/codawin.pdf?dl=0).

Coda won an award at Linux World in 1999.  Coda unequestionably influenced systems like IntelliMirror and Dropbox, but Coda was difficult to productize.   Towards 1998, I became interested in simpler light weight solutions.

### 1998-2001 InterMezzo

InterMezzo was a new synchronizing file system, with a driver called  *presto* which was a Linux kernel file system (until I requested it to be removed because I was unable to take it further, as Lustre had taken over my life).  It worked with a daemon in user space named Vivace (FUSE didn't exist yet).  It shared the key feature of disconnected operation with Coda.  It's focus was simplicity and efficiency, and indeed its core functionality was implemented in a few 1000 lines of kernel and user level code.   It was used by Mountain View Data as the basis of a product, and by Tacit Systems.  

I wrote several papers about InterMezzo

 - [Removing Bottlenecks in Distributed Filesystems:Coda & InterMezzo as examples](https://www.dropbox.com/s/w6lefx8sk25v7kf/bottlenecks%20paper.pdf?dl=1)
 - [Opening Bottlenecks in Distributed File Systems](https://www.dropbox.com/s/40kbz155f8fec7a/bottlenecks%20talk.pdf?dl=0)
 - [InterMezzo: File Synchronization with InterSync](https://www.dropbox.com/s/jz5q62ph6d3vnqa/is.pdf?dl=0)
 - [InterMezzo File System:  Synchronizing Folder Collections Stelias Computing Inc](https://www.dropbox.com/s/if645arol12bwhx/syncwhitenew.pdf?dl=0)

Around this time I compared the protocols used by many network file system, and an overview of some of my thoughts at that time is contained in this presentation given at the Usenix Technical Conference 

 - [File Systems for Clusters](https://www.dropbox.com/s/4j1xh2o9cay0vul/clusterdfs99.pdf?dl=0)


### 1999-2013 Lustre

Lustre, a parallel file system for HPC, is the most successful project I started.   Now, 20 years after I started it, it's still very widely used, and acquisitions of the Lustre team which currently mostly operates out of Whamcloud, a DDN owned company, continue to happen.  The later history is well documented on wikipedia.

I wrote a lengthy document, dubbed [the Lustre book](https://www.dropbox.com/s/w3ldnnx87r2v498/lustrebook-20050929.pdf?dl=0) about Lustre's architecture, which described the large set of features mostly requested by the DOE, usually leveraging the lines of thought in my earlier work. Even relatively recent features such as client metadata write back caching were described in this book.  In some cases, for example that of quality of service, the design of the book (which I developed with Sandia National Laboratories) was not implemented.  Amusingly my staff said that the best book written about Lustre was the NFS v4.1 spec, which does indeed overlap with Lustre (without ever mentioning it).

Several white papers contain key elements of the architecture.  These white papers were widely distributed in the 2000's, we include several here:

 - [Lustre® File System](https://www.dropbox.com/s/ibo3mn3k5r0fv2u/lustrefilesystem_whitepaper_v2.pdf?dl=0)

 - [Peta-Scale I/O with the Lustre® File System](https://www.dropbox.com/s/a7btkim91mv0l1o/2007-08%20peta_scale_io_white_paper_v1_1.pdf?dl=0)

 - [Lustre® Networking](https://www.dropbox.com/s/svifbkdlydldwib/2007-07%20LustreNetworking_WhitePaper_v4_1.pdf?dl=0)

 - [Cluster File Systems Technical Strategy](https://www.dropbox.com/s/g1cha056r358fqq/2007-05-TechnicalStrategy_WhitePaper_v2_Final.pdf?dl=0)

 - [Lustreô HSM functionality ](https://www.dropbox.com/s/wzhazgko4goxb83/2004-12-lustrehsm-1.4.pdf?dl=0)

 - [Lustre Configuration](https://www.dropbox.com/s/vqpycbsnk6vj4w7/Mountconfig.pdf?dl=0)

Perhaps just 3 ideas in Lustre truly stand out as original: 

 - It used a very compact RPC format that minimized round trips, called "intents".  A similar mechanism was later incorporated into NFSv4.
 - A very simple "commit callback" mechanism was used to inform cache management that persistent state had been reached.  To this day I remain surprised that commit callbacks were not widely used in storage applications.
 - Lustre has sophisticated management of request sequences to reach almost exactly once semantics for remote procedure calls.  Again, similar mechanisms were later incorporated into NFSv4.
 - An "epoch recovery" model was developed and patented in the mid 2000's. It underlies DAOS recovery.

A few other patents were awarded to me, mostly about detailed optimization and consistency mechanisms.

### 2009-2013 Colibri, later Mero

Colibri was a design created in 2009 for an "exa-scale" container based storage system.  It was developed in my startup ClusterStor which was acquired by Xyratex (which in turn became part of Seagate, after which Seagate sold other assets of ClusterStor to Cray).  Colibri was renamed to Mero and briefly became a product for Seagate.  

Colibri could not pursue an open source model, and the details of its architecture have been carefully guarded by its owner.   A few presentations describe its key thoughts, and we attach one here delivered in 2010 at the TeraTec Conference.
  [Exascale File Systems, Scalability in ClusterStor’sColibri System](https://www.dropbox.com/s/n8l0pvv0tkazsvb/A5_Braam_ClusterStor_Forum_Teratec_2010.pdf?dl=0)

### 2010-2013 Exascale IO effort

The ExaScale IO effort was a discussion group I led from 2011 - 2013 in which many experts participated.  When I left Xyratex, others took over the effort.  It influenced other projects, and a European research project SAGE resulted from several more years of exploration.

White paper and 4 progress reports on EIO.

 - [The Exa-scale I/O initiative - EIOW](https://www.dropbox.com/s/i63gj997in9fuh4/Exascale%20IO%20-%20EIOW%20-%20Peter%20Braam.pdf?dl=0)
 - [EIOW Workshop](https://www.dropbox.com/s/yg4h44gk3filuqx/2012-05-eiow-workgroup.pdf?dl=0)
 - [EIOW Data access, management and storage: the road ahead](https://www.dropbox.com/s/z0h2c2vi1ui3hpk/2012-12-EIOW-HIPC.pdf?dl=0)
 - [EIOW – Exa-scale I/O workgroup](https://www.dropbox.com/s/3205z17ld326bbb/2013-03-EIOW-HLRS.pdf?dl=0)

### 2012 Secure Lustre

The Lustre book described an architecture for security in Lustre.  In 2013 Lockheed Martin awarded a contract to Xyratex to implement this design, and a secure Lustre file system became a product.  My suspicion is that, under the influence of cloud infrastructure, the security considerations have significantly changed since these approaches were explored.

### 2016-2018 Campaign Storage

Campaign Storage is an archival storage system designed and implemented at Los Alamos National Laboratory (LANL).  The effort took place in Gary Grider's group.  Nathan Thompson and I explored this commercially, but thought its target market was too small.  

Gary Grider and I discussed this from an open source perspective.  During this process, I organized a Campaign Storage discussion group which attracted reasonable attendance.  LANL and I prepared a response to an official RFI regarding these matters.  Attached are a few papers and presentations regarding this. 

 - [RFI - request](https://www.dropbox.com/s/8g1nutwpgmvlmgh/RFI-DASS-032818_v0.9_Revision_1_redline.pdf?dl=0) and its [technical attachment](https://www.dropbox.com/s/nruxsx76uaguzf6/Attachment_A_-_RFI-DASS-032818.pdf?dl=0) 
 - [RFI Response Campaign Storage for Archiving](https://www.dropbox.com/s/zk1t4q3shpevmqv/2018-05-25%20RFI-DASS-032818_Response%20from%20LANL%20%26%20braamio_Campaign%20Storage.pdf?dl=0) 
 - [Campaign Storage MSST Paper](https://www.dropbox.com/s/r9y3lo0iyblwqa2/2017-04-27-campaign-storage-msst%20.pdf?dl=0) 

{{% /col %}}

{{% /cols %}}


