---
title: "Mathematics"
date: Sun Sep 16 03:39:34 2018
draft: false
authors: 
  - Peter Braam
---
{{% cols %}}
{{% col %}}
{{< figure src="img/math.jpg" width="500px" >}}
{{% /col %}}

{{% col %}}
I remain very interested in mathematics.

In my opinion, one particularly beautiful paper is that of Graeme Segal entitled [Space and Spaces](https://www.lms.ac.uk/sites/lms.ac.uk/files/files/About_Us/AGM_talk.pdf).
{{% /col %}}

{{% /cols %}}
