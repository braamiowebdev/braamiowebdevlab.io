---
title: "SKA"
date: 2018-08-07T20:25:37+10:00
draft: false
authors: 
  - Peter Braam
show_gallery: false
resources:
  - src: "img/ska_day.png"
    title: 
    params:
---

# Science Data Processor for the Square Kilometer Array Radio Telescope

I have been involved in the SKA Science Data Processor (SDP) project since 2013.   I worked in the following areas:

### Explore data-driven programming models

An early memo I co-authored with Bojan Nikolic explains some of the approaches.   My company was contracted to explore multiple approaches to domain specific languages and data flow approaches.  We first created several prototypes in Haskell, then explored other domain specific languages that had been successful (in particular Halide and Legion).

We reviewed Legion in detail, and while Legion's architecture appeared a very good match, its implementation wasn't far enough along for it to get the traction we were hoping for.

The project is described in the following documents:

 - [DSL computing for SKA: Proposal](https://www.dropbox.com/s/m7erghvieafd3j3/2014-05-09%20DSL%20computing%20for%20SKA%20Proposal%20%20-Braam%20Research.pdf?dl=0) 

 - This contract was awarded, and we include the Milestone reports: 
	- Milestone 1: [Evaluating Data Flow DSL’s for the SKA SDP](https://www.dropbox.com/s/i6jc00xxp7zczfz/MS1%20finalreport.pdf?dl=0)
	- Milestone 2: [SKA SDP DSL project](https://www.dropbox.com/s/3by1d4qyxcm6j3q/MS2%20final%20report.pdf?dl=0), [DSL Project Milestone Design](https://www.dropbox.com/s/jehrk3jeu8way0d/MS2%20DSL%20Project%20Milestone%20Design.pdf?dl=0) 
	- Milestone 3: [SDP DSL project report](https://www.dropbox.com/s/4b386vuslefhbwu/SDP%20DSL%20project-milestonel%203%20final%20report.pdf?dl=0), [DNA Performance Report](https://www.dropbox.com/s/ssa87n2upse0zrr/MS3%20DNA%20performance%20report.pdf?dl=0), [DNA API Manual](https://www.dropbox.com/s/51csg4qdjqf00xb/MS3%20DNA%20API%20manual.pdf?dl=0)
  	- Milestone 4: [Synthesis as Data Flow](https://www.dropbox.com/s/0zp9sfcg7d4qofl/MS4%20-%20Final%20Report%202_%20Synthesis%20as%20Data%20Flow.pdf?dl=0), [Comparison of HPC data flow languages forSKA SDP](https://www.dropbox.com/s/qxc9ynjby5tvvzt/MS4%20Comparison%20of%20HPC%20data%20flow%20languages%20for%20SKA%20SDP.pdf?dl=0)
	- Milestone 5: [Pipeline DSL](https://www.dropbox.com/s/47fq1hvk6zuyatk/Milestone%205%20-%20final%20report_%20pipeline%20DSL.pdf?dl=0), [MS5 DSL Design](https://www.dropbox.com/s/b5ptv02tr56gqgk/MS5%20DSL%20design.pdf?dl=0)
	- Milestone 6: [DSL Continuum Pipeline](https://www.dropbox.com/s/2jzqdkwrc4eykfy/MS6%20Final%20Report%20PB%20-%20DSL%20Continuum%20Pipeline.pdf?dl=0)
	- Milestone 7: [Data Flow Prototyping Report](https://www.dropbox.com/s/qqbwnbxllm6t208/ska-tel-sdp-0000083_c_rep_sdpmemodataflowprototyping_-_signed.pdf?dl=0) 
- Deep study of Legion - part of Milestone 7

There is an accompanying [Radio Cortex GitHub repository](https://github.com/SKA-ScienceDataProcessor/RC)

Working with the Haskell community to get a solid grasp of the programming principles and with teams around the world understanding their approaches to HPC was extremely interesting.

###  Create industrial relationships

I established high level executive relationships between SKA and major chip and memory vendors.  The resulting discussions were very influential, particularly with regards to understanding memory technologies.

###  Software Engineering Process for SKA 

I wrote a memo explaining the methodology from Carnegie Mellon's Software Engineering Institute, with which I'm very familiar, having used it in startups and big corporations.  It was adopted by the entire SKA organization.

[A preliminary Software Engineering Process for SKA](https://www.dropbox.com/s/4nq0f4wi6qjqvv0/SDP%20Preliminary%20Software%20Engineering.pdf?dl=0)

### Review data storage software.

This is of course an ongoing discussion, as always driven as much by opinions and politics as facts.  A memo summarizes my thoughts.

[IO and Storage Software](https://www.dropbox.com/s/eotdcjijdja9j0r/2017-12_sdp_io_and_storage_software_part_1_-_signed.pdf?dl=0)

### Strategic use of variable precision number formats and arithmetic

There is much interest in new number formats, for example Google's TPU processor incorporates an implementation called bfloats.   A much more general approach has been designed by John Gustafson see [Posithub.org](https://www.posithub.org).  

Watch my blog for my keynote presentation regarding this topic at CONGA and for our report.

